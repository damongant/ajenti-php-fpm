from ajenti.api import *
from ajenti.plugins import *

info = PluginInfo(
    title='PHP FPM',
    icon='globe',
    dependencies=[
        PluginDependency('webserver_common')
    ],
)


def init():
    import main


